app.controller('loginController', ['$rootScope', '$scope', '$cookies', '$location', 'buyerAuthService', 'alerts', function($rootScope, $scope, $cookies, $location, buyerAuthService, alerts) {

	$scope.waiting = false;
	$scope.loginForm = {email: '', password: '', rememberMe: true};

	$scope.login = function() {
		$scope.waiting = true;
		buyerAuthService.login($scope.loginForm.email, $scope.loginForm.password, $scope.loginForm.rememberMe)
		.then(function(response) {
			if(response.status === 202) {
				$cookies.put('aj', response.headers('authorization'));
				if($location.search().rp !== undefined && $location.search().rp.length > 0) {
					window.location = $location.search().rp;
				}else {
					window.location = '/';
				}
			}else {
				alerts.warning('Your email or password incorrect!');
			}
			$scope.waiting = false;
			$scope.loginForm.password = '';
		}, function() {
			$scope.waiting = false;
			$scope.loginForm.password = '';
			alerts.warning('Your email or password incorrect!');
		});
	};

}]);
