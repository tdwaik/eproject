app.controller('addressesController', ['$scope', 'addressService', 'alerts', function ($scope, addressService, alerts) {
    $scope.buyerAddresses = [];
    $scope.newAddress = {};
    $scope.countries = {};
    $scope.cities = {};
    $scope.showNewAddress = false;
    $scope.showEditAddress = false;
    $scope.editAddressId = 0;

    $scope.addAddress = function(newAddress) {
        $scope.buyerAddresses[$scope.buyerAddresses.length] = newAddress;
        alerts.success('You\'ve Add New Address');
        $scope.showNewAddress = false;
        $scope.showEditAddress = false;
    };

    $scope.showNewAddressForm = function() {
        $scope.showNewAddress = true;
        $scope.showEditAddress = false;
    };

    $scope.editAddress = function(addressId) {
        $scope.editAddressId = addressId;
        $scope.showNewAddress = false;
        $scope.showEditAddress = true;
    };

    $scope.updateAddress = function(updatedAddress) {
        console.log(updatedAddress);
    };

    $scope.deleteAddress = function(addressId, index) {
        if(confirm("Are you sure you want to delete this Address?")) {
            addressService.deleteAddress(addressId).then(function (response) {
                if (response.status === 202) {
                    delete $scope.buyerAddresses.splice(index, 1);
                    alerts.success('Address Deleted');
                } else {
                    alerts.oops();
                }
            }, function() {
                alerts.oops();
            });
        }
    };
    
    $scope.makeAddressPrimary = function(addressId, index) {
        addressService.makeAddressPrimary(addressId).then(function(response) {
            if (response.status === 202) {
                var primeIndex = _.findLastIndex($scope.buyerAddresses, {isPrimary : true});
                $scope.buyerAddresses[primeIndex].isPrimary = false;
                $scope.buyerAddresses[index].isPrimary = true;
            }
        }, function() {
            alerts.oops();
        });
    };

    addressService.getAuthBuyerAddresses().then(function(response) {
        $scope.buyerAddresses = response.data;
        $scope.showNewAddress = _.size($scope.buyerAddresses) == 0;
    },function() {
        alerts.oops();
    });

}]);