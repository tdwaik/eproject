app.controller('checkoutController', ['$scope', '$route', 'cartServices', 'addressService', 'alerts', function($scope, $route, cartServices, addressService, alerts) {
    $scope.cartOffers = [];
    $scope.buyerAddresses = [];
    $scope.orderSummary = [];
    $scope.offersQuantity = [];

    cartServices.getCartOffers().then(function(response) {
        $scope.cartOffers = response.data;
    }, function(response) {
        alerts.oops();
    });

    cartServices.getOrderSummary().then(function(response) {
        $scope.orderSummary = response.data;
    }, function(response) {
        alerts.oops();
    });

    addressService.getAuthBuyerAddresses().then(function(response) {
        $scope.buyerAddresses = response.data;
    }, function(response) {
        alerts.oops();
    });

}]);