/**
 * Created by Thaer Aldwaik on February 28, 2016
 */

app.controller('offerDetailsController', [
	'$rootScope', 
	'$scope', 
	'$routeParams', 
	'$location', 
	'$filter',
	'$route',
	'offersService', 
	'sizesService',
	'cartServices',
	'alerts',
	function($rootScope, $scope, $routeParams, $location, $filter, $route, offersService, sizesService, cartServices, alerts) {

	var queryParam = {};
	
	var init = function() {
		$scope.offerDetails = {};
		$scope.selectedSize = 0;
		$scope.selectedPrice = 0;
		$scope.minPrice = null;
		$scope.maxPrice = null;
		$scope.stockQuantity = 0;
		$scope.pictureViewdKey = 0;
		$scope.categorySizes = {};
		$scope.offerSizesQuantity = [];
		$scope.quantity = 1;
		$scope.selectedStockId = 0;
	};

	var getPictures = function(name, count) {
		var pictures = Array();
		
		// pictures['MC'] = $filter('imageFilter')(name, $scope.SImage, 0);
		
		for(var i = 0; i < count; i++) {
			pictures[i] = $filter('imageFilter')(name, $scope.SImage, i);
		}

		return pictures;
	};

	var updateQueryParams = function(param, value) {
		queryParam[param] = value;
		$location.search(queryParam);
	};

	$scope.changeSize = function(sizeId) {
		$scope.selectedSize = sizeId;
		angular.forEach($scope.offerDetails.offerStockList, function(stock) {
			if(stock.sizeId == sizeId && stock.stockQuantity > 0) {
				$scope.selectedPrice = stock.price;
				$scope.stockQuantity = stock.stockQuantity;
				$scope.selectedStockId = stock.id;
				updateQueryParams('s', sizeId);
			}
		});
	};

	$scope.updateVariation = function(variationId) {
		$route.updateParams({variationId: variationId});
		offersService.getOfferDetails($routeParams.offerId, variationId).then(function(response) {
			init();
			$scope.offerDetails = response.data;

			var offerStock = response.data.offerStockList;
			angular.forEach(offerStock, function(stock) {
				$scope.minPrice = ($scope.minPrice == null || stock.price < $scope.minPrice)? stock.price : $scope.minPrice;
				$scope.maxPrice = ($scope.minPrice == null || stock.price > $scope.maxPrice)? stock.price : $scope.maxPrice;
				$scope.offerSizesQuantity[stock.sizeId] = stock.stockQuantity;
			});

			if($scope.minPrice == $scope.maxPrice) {
				$scope.selectedPrice = $scope.maxPrice;
			}

			sizesService.getSizesByCategoryId(response.data.categoryId).then(function(response) {
				$scope.categorySizes = response.data;
			});

			$scope.pictures = getPictures($scope.offerDetails.picture, $scope.offerDetails.totalPictures);

			if($location.search().s > 0) {
				$scope.changeSize($location.search().s);
			}

		}, function(response) {
			alerts.oops();
		});
	};

	$scope.updatePicture = function(imageNumber) {
		$scope.pictureViewdKey = imageNumber;
	};

	$scope.getSizeClass = function(sizesId) {
		if($scope.offerSizesQuantity[sizesId] === undefined || $scope.offerSizesQuantity[sizesId] < 1) {
			return 'disabled';
		}else if($scope.selectedSize == sizesId) {
			return 'active';
		}
	};

	$scope.addToCart = function () {
		if($scope.selectedStockId < 1) {
			alerts.warning('Please select size first');
			return false;
		}

		cartServices.addToCart($scope.selectedStockId, $scope.quantity).then(function(response) {
			if(response.status == 201) {
				alerts.success('Offer added to your cart');
			}else {
				alerts.oops();
			}
		}, function() {
			alerts.oops();
		});
	};

	if($routeParams.offerId > 1 && $routeParams.variationId > 1) {
		$scope.updateVariation($routeParams.variationId);
	}else {
		$location.path('/notFound');
	}

}]);

