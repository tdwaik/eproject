/**
 * Created by Thaer Aldwaik on January 1, 2016
 */

app.controller('offersListController', ['$scope', '$routeParams', 'offersService', 'alerts', function($scope, $routeParams, offersService, alerts) {

	$scope.offers = {};

	$scope.getLastOffers = function() {
		offersService.getOffersList().then(function(response) {
			if(response.status === 200) {
				$scope.offers = response.data;
			}
		}, function(response) {
			alerts.oops();
		});
	};

	$scope.getLastOffers();

}]);
