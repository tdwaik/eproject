app.controller('signUpController', ['$scope', 'signUpService', 'alerts', function($scope, signUpService, alerts) {

	$scope.signUpForm = {};

	$scope.signUp = function() {

		if($scope.signUpForm.email !== $scope.signUpForm.emailConfirm) {
			alerts.warning('email don\'t match');
			return false;
		}

		if($scope.signUpForm.password.length < 6) {
			alerts.warning('Passwords must be at least 6 characters');
			return false;
		}

		signUpService.signUp($scope.signUpForm)
		.then(function(response) {
			if(response.status == 201) {
				alerts.success('Welcome, Your account created');
			}
		}, function(response) {
			if(response.status == 400) {
				alerts.warning('Please fill required fields');
			}else if(response.status == 409){
				alerts.warning('Email already exists');
			}else {
				alerts.oops();
			}
		});
	};

}]);
