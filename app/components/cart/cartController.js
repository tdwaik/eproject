app.controller('cartController', ['$scope', '$route', 'cartServices', 'addressService', 'alerts', function($scope, $route, cartServices, addressService, alerts) {
    $scope.cartOffers = [];
    $scope.buyerAddresses = [];
    $scope.orderSummary = [];
    $scope.offersQuantity = [];

    $scope.removeFromCart = function(cartId) {
        cartServices.removeFromCart(cartId).then(function(response) {
            $route.reload();
        }, function(response) {

        });
    };

    cartServices.getCartOffers().then(function(response) {
        $scope.cartOffers = response.data;
    }, function(response) {

    });

    cartServices.getOrderSummary().then(function(response) {
        $scope.orderSummary = response.data;
    }, function(response) {

    });

    addressService.getAuthBuyerAddresses().then(function(response) {
        $scope.buyerAddresses = response.data;
    }, function(response) {

    });

    $scope.updateQuantity = function(cartId) {
        cartServices.updateQuantity(cartId, $scope.offersQuantity[cartId]).then(function(response) {
            alerts.success('Quantity Updated');
        }, function(response) {
            alerts.oops();
        });
    }

}]);