/**
 * Created by Thaer Aldwaik on February 13, 2016
 */

app.controller('mainController', ['$rootScope', function($rootScope) {

    $rootScope.alerts = [];

    $rootScope.pushAlert = function(type, message) {
        $rootScope.alerts.push({type: type, message: message});
    };

    $rootScope.closeAlert = function(index) {
        $rootScope.alerts.slice(1, index);
    };

}]);