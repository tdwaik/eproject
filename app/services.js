var APIEndPoint = 'https://s.wearwiki.com';

app.service('API', ['$http', function($http) {
	return {
		call: function(method, url, data) {
			return $http({
				method: method,
				url: APIEndPoint + url,
				data: data
			});
		}
	}
}]);

app.service('buyersService', ['API', function(API) {
	this.getAuthBuyer = function() {
		return API.call('get', '/buyers/authBuyer');
	};
}]);

app.service('buyerAuthService', ['$rootScope', '$cookies', '$location', 'API', function($rootScope, $cookies, $location, API) {
	this.login = function(email, password, rememberMe) {
		return API.call('post', '/buyerAuth/login', {email: email, password: password, rememberMe: rememberMe});
	};

	this.logout = function() {
		$cookies.remove("aj");
		$rootScope.isLogin = false;
		window.location = '/';
	};

	this.checkLoginStatus = function() {
		return API.call('get', '/buyerAuth/isLogin');
	};
}]);

app.service('signUpService', ['API', function(API) {
	this.signUp = function(signUpForm) {
		return API.call('put', '/buyers/addBuyer', signUpForm);
	};

}]);

app.service('offersService', ['API', function(API) {
	this.getOffersList = function() {
		return API.call('get', '/offers/list/desc/0/10');
	};

	this.getOfferDetails = function(offerId, offerOptionId) {
		return API.call('get', '/offers/details/' + offerId + '/' + offerOptionId);
	};
}]);

app.service('sizesService', ['API', function(API) {
	this.getSizesByCategoryId = function(categoryId) {
		return API.call('get', '/sizes/getSizesByCategoryId/' + categoryId);
	};
}]);

app.service('countriesService', ['API', function(API) {
	this.getCountries = function() {
		return API.call('get', '/countries');
	}
}]);

app.service('citiesService', ['API', function(API) {
	this.getCitiesByCountryId = function(countryId) {
		return API.call('get', '/cities/citiesByCountry/' + countryId);
	};
}]);

app.service('addressService', ['API', function(API) {
	this.addAddress = function(addressJson) {
		return API.call('PUT', '/addresses', {addressJson: addressJson});
	};

	this.getAuthBuyerAddresses = function() {
		return API.call('GET', '/addresses');
	};

	this.getAddressById = function(addressId) {
		return API.call('GET', '/addresses/' + addressId);
	};

	this.deleteAddress = function(addressId) {
		return API.call('DELETE', '/addresses/' + addressId);
	};

	this.makeAddressPrimary = function(addressId) {
		return API.call('GET', '/addresses/makeAddressPrimary/' + addressId);
	};
}]);

app.service('cartServices', ['API', function(API) {
	this.getCartOffers = function() {
		return API.call('GET', '/cart');
	};

	this.addToCart = function(offerStockId, quantity) {
		return API.call('PUT', '/cart', {offerStockId: offerStockId, quantity: quantity});
	};

	this.removeFromCart = function(cartId) {
		return API.call('DELETE', '/cart/remove/' + cartId);
	};

	this.getOrderSummary = function() {
		return API.call('GET', '/cart/orderSummary');
	};

	this.updateQuantity = function(cartId, quantity) {
		return API.call('POST', '/cart/updateQuantity', {cartId: cartId, quantity: quantity});
	};
}]);