app.filter('imageFilter', ['$rootScope', function($rootScope) {
	return function(input, size, number) {
		number = number == 0? 'MC' : number;
		var imageSrc = $rootScope.imageCDN + 'offers/' + size + '/' + number + '_' + input;

		return imageSrc;
	};
}]);