/**
 * Created by Thaer Aldwaik on February 29, 2016
 */

app.directive('pageHeader', function () {

	var controller = ['$scope', 'buyersService', 'buyerAuthService', '$location', 'alerts', function($scope, buyersService, buyerAuthService, $location, alerts) {
		$scope.authBuyer = {};

		$scope.$watch('isLogin', function(isLogin) {
			if(isLogin) {
				buyersService.getAuthBuyer().then(function(response) {
					$scope.authBuyer = response.data;
				}, function(response) {
					if(response.status != 401) {
						alerts.oops();
					}
				});
			}
		});

		$scope.locationPath = function() {
			return $location.path() != '/login'? $location.path() : '';
		};

		$scope.logout = function() {
			buyerAuthService.logout();
		}
	}];

	return {
		restrict: 'E',
		templateUrl: '/app/directives/header/header.html',
		controller: controller
	};
});

