/**
 * Created by Thaer Aldwaik on February 29, 2016
 */

app.directive('pageFooter', function () {
	return {
		restrict: 'E',
		templateUrl: '/app/directives/footer/footer.html'
	};
});

