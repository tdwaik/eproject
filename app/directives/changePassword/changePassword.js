app.directive('changePassword', function() {
    var controller = ['$scope', function($scope) {
        $scope.changPassword = function() {}
    }];

    return {
        templateUrl: '/app/directives/changePassword/changePassword.html',
        controller: controller
    }
});