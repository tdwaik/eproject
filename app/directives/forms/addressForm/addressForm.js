app.directive('addressForm', function() {

    var controller = ['$scope', 'countriesService', 'citiesService', 'addressService', 'alerts', function($scope, countriesService, citiesService, addressService, alerts) {

        $scope.address = {};
        $scope.address.countryId = 0;

        $scope.submit = function() {
            $scope.formType == 'edit'? updateAddress() : addAddress();
        };

        $scope.updateCitiesDropdown = function() {
            citiesService.getCitiesByCountryId($scope.address.countryId).then(function(response) {
                $scope.cities = response.data;
            }, function(response) {
                alerts.oops();
            });
        };

        var addAddress = function() {
            addressService.addAddress(JSON.stringify($scope.address)).then(function(response) {
                if(response.status === 201) {
                    if (_.isFunction($scope.callBack)) {
                        $scope.address.id = response.data;
                        $scope.callBack({newAddress: $scope.address});
                    }
                }else {
                    alerts.oops();
                }
            }, function(response) {
                alerts.oops();
            });
        };

        var updateAddress = function() {
            addressService.updateAddress(JSON.stringify($scope.address)).then(function(response) {
                if(response.status === 202) {
                    if (_.isFunction($scope.callBack)) {
                        $scope.callBack({updatedAddress: $scope.address});
                    }
                }else {
                    alerts.oops();
                }
            }, function(response) {
                alerts.oops();
            });
        };

        countriesService.getCountries().then(function(response) {
            $scope.countries = response.data;
        }, function(response) {
            alerts.oops();
        });

        if($scope.formType == 'edit') {
            $scope.$watch("editAddressId", function(editAddressId) {
                addressService.getAddressById(editAddressId).then(function(response) {
                    if (response.status == 200) {
                        $scope.address = response.data;
                        $scope.updateCitiesDropdown();
                    } else {
                        alerts.oops();
                    }
                }, function (response) {
                    alerts.oops();
                });
            });
        }

    }];

    return {
        scope: {
            callBack: '&',
            formType: '@',
            editAddressId: '='
        },
        compile: function(element, attrs) {
            if(!attrs.callBack) { attrs.callBack = function() {} }
            if(!attrs.formType) { attrs.formType = 'add' }
            if(!attrs.editAddressId) { attrs.editAddressId = 0 }
        },
        templateUrl: '/app/directives/forms/addressForm/addressForm.html',
        controller: controller
    }
});