/**
 * Created by Thaer Aldwaik on January 26, 2016
 */

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/app/components/home/homeView.html',
            controller: 'homeController'
        })
        .when('/c/:param1', {
            templateUrl: '/app/components/offersList/offersList.html',
            controller: 'offersListController'
        })
        .when('/offer/:offerId/:variationId/:offerTitle', {
            templateUrl: '/app/components/offerDetails/offerDetailsView.html',
            controller: 'offerDetailsController'
        })
        .when('/contact', {
            templateUrl: '/app/components/contact/contactView.html'
        })
        .when('/login', {
            templateUrl: '/app/components/login/login.html',
            controller: 'loginController',
            // resolve:{
            //   "check":function($rootScope, $location) {
            //     if($rootScope.isLogin) {
            //       $location.path('/');
            //     }
            //   }
            // }
        })
        .when('/signup', {
            templateUrl: '/app/components/signUp/signUp.html',
            controller: 'signUpController',
            // resolve:{
            //   "check":function($rootScope, $location) {
            //     if($rootScope.isLogin) {
            //       $location.path('/');
            //     }
            //   }
            // }
        })
        .when('/account', {
            templateUrl: '/app/components/account/account.html',
            controller: 'accountController'
        })
        .when('/orders', {
            templateUrl: '/app/components/orders/orders.html',
            controller: 'ordersController'
        })
        .when('/personal-info', {
            templateUrl: '/app/components/account/account.html',
            controller: 'accountController'
        })
        .when('/addresses', {
            templateUrl: '/app/components/addresses/addresses.html',
            controller: 'addressesController'
        })
        .when('/wish-list', {
            templateUrl: '/app/components/wishList/wishList.html',
            controller: 'wishListController'
        })
        .when('/cart', {
            templateUrl: '/app/components/cart/cart.html',
            controller: 'cartController'
        })
        .when('/checkout', {
            templateUrl: '/app/components/checkout/checkout.html',
            controller: 'checkoutController'
        })
        .when('/notFound', {
            templateUrl: '/app/components/404/404.html'
        })
        .otherwise({
            redirectTo: '/notFound'
        });

    // configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode({
        enabled: true
    });

}]);


