/**
 * Created by Thaer Aldwaik on January 1, 2016
 */

"use strict";

var app = angular.module("app",
    [
        'ngRoute',
        'ngCookies',
        'ngSanitize',
        'ngMessages',
    ]
);

app.config(['$httpProvider', function($httpProvider) {
	// Use x-www-form-urlencoded Content-Type
  var contentType = 'application/x-www-form-urlencoded;charset=utf-8';
  
  $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
  $httpProvider.defaults.headers.post['Content-Type'] = contentType;
  $httpProvider.defaults.headers.put['Content-Type'] = contentType;

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data)
  {
    /**
     * The workhorse; converts an object to x-www-form-urlencoded serialization.
     * @param {Object} obj
     * @return {String}
     */ 
    var param = function(obj)
    {
      var query = '';
      var name, value, fullSubName, subName, subValue, innerObj, i;
      
      for(name in obj)
      {
        value = obj[name];
        
        if(value instanceof Array)
        {
          for(i=0; i<value.length; ++i)
          {
            subValue = value[i];
            fullSubName = name + '[' + i + ']';
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += param(innerObj) + '&';
          }
        }
        else if(value instanceof Object)
        {
          for(subName in value)
          {
            subValue = value[subName];
            fullSubName = name + '[' + subName + ']';
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += param(innerObj) + '&';
          }
        }
        else if(value !== undefined && value !== null)
        {
          query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }
      }
      
      return query.length ? query.substr(0, query.length - 1) : query;
    };
    
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
}]).run(['$rootScope', '$http', '$cookies', 'buyerAuthService', function($rootScope, $http, $cookies, buyerAuthService) {
	$http.defaults.headers.common['Authorization'] = $cookies.get('aj');

  $rootScope.isLogin = null;

  $rootScope.imageCDN  = 'https://cdn.wearwiki.com/images/';
  $rootScope.XLImage   = 'XL';
  $rootScope.LImage    = 'L';
  $rootScope.MImage    = 'M';
  $rootScope.SImage    = 'S';
  $rootScope.mainCover = 'MC';
  $rootScope.original  = 'O';

  buyerAuthService.checkLoginStatus().then(function(response) {
    if(response.status === 200) {
      $rootScope.isLogin = true;
    }else {
      $rootScope.isLogin = false;
    }
  }, function() {
    $rootScope.isLogin = false;
  });

}]);

app.service('settings');

if (!Array.isArray) {
  Array.isArray = function(arg) {
    return Object.prototype.toString.call(arg) === '[object Array]';
  };
}
